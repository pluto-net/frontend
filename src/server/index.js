const express = require("express");
const app = express();

const sentencePool = [
  "Decentralized Scholarly Communication",
  "Breaking down barriers in academia",
  "Scholarly Communication for Researchers, Powered by Researcher",
  "Blockchain Technology takes Scholarly Communication to Whole New Level",
  "DO RESEARCH, NEVER RE-SEARCH",
  "sciNapse is a free, nonprofit, Academic search engine for papers, serviced by Pluto Network.",
];

app.get("/notifications", (req, res) => {
  const chanceToSendNotification = Math.random() * 10 < 1; // You can change this chance for test

  if (chanceToSendNotification) {
    const randomIndexForTitle = Math.floor(Math.random() * 6);
    const title = sentencePool[randomIndexForTitle];
    const randomIndexForContent = Math.floor(Math.random() * 6);
    const content = sentencePool[randomIndexForContent];

    const currentDate = new Date();
    const currentDateString = currentDate.toString();

    const message = {
      title: title,
      content: content,
      date: currentDateString,
    };

    return res.send(JSON.stringify(message));
  }
  return res.send("Hello World!");
});

app.listen(3000, () =>
  console.log("Pluto FrontEnd notification generating server is now working on localhost:3000! ⚡"),
);
