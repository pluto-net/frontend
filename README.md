# Pluto FrontEnd Test

## Goal

![notification image](https://i.imgur.com/lKYQcMj.png)

Make a simple notification module like the desktop Chrome notification.

## Requirements specification

* The notifications must be stacked in the order they come first.
* The notification must disappear in 10 seconds later and the maximum count of the notifications to show is 3.
* The notification has close button and if an user click it, the button disappear immediately.
* If the notification disappear and there are more notifications, the left ones should fill the space.
* You should get notification from local server per every 1 second.
* The notification should contain and show `title`, `content`, `date`.

## Technical Restrictions

* You can use any version of Javascript. It means that you can use ES6, ES2015, TypeScript, ClojureScript too.
* From above if you need Transpilers(Babel, Typescript, ...) or other bundling managers(Webpack, Parcel, Rollup, ...), it's totally okay to use.

## ETC

* The UI design of the notification system doesn't matter at all.
* If you can change current notification system to using Web Socket and handle both server and client side, you will get extra point.

## How to start local server

```js
npm install
npm start
```

Then the local server will run at 3000 port.  
visit `http://localhost:3000` or `0.0.0.0:3000` or `127.0.0.1:3000`
